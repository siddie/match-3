import Application from './js/app/Application';

const OPTIONS = {
  GRID: {
    WIDTH: 10,
    HEIGHT: 8
  },
  JEWELS_TYPES: [
    { color: "RED" },
    { color: "GREEN" },
    { color: "BLUE" }
  ]
};

document.addEventListener('DOMContentLoaded', function(){
  void new Application( OPTIONS );
});
