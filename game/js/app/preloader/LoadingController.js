import LoadingScreen from './LoadingScreen';
import Preloader from './AssetsPreloader';

class LoadingController {
  constructor() {
    this.screen = new LoadingScreen();
    this.preloader = new Preloader();

    this.screen.show();
  }

  load = () => new Promise(async (resolve) => {
    const assets = await this.preloader.loadResources();
    this.assets = assets;
    await this.screen.hide();
    resolve(assets);
  });
}

export default LoadingController;
