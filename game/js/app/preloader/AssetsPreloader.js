import * as PIXI from 'pixi.js';
import assetsModel from './assets.json';

/**
 * Its a proxy between native pixi loader and this application
 */
class Preloader {
  constructor() {
    this.model = assetsModel;
    this.loader = new PIXI.Loader();
  }

  loadResources = () => new Promise((resolve) => {
    this.loader.on('complete', loader => resolve(loader.resources));

    /**
     * Parse model and add each object to pixi loader
     */
    for (let i = 0; i < this.model.length; ++i) {
      const { name, path } = this.model[i];
      this.loader.add(name, path);
    }
    
    this.loader.load();
  });
}

export default Preloader;
