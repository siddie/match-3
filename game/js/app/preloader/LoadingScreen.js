import * as PIXI from 'pixi.js';
import { TimelineLite } from 'gsap/TweenMax';
import COLORS from 'utils/colors';

class LoadingScreen {
  constructor() {
    this.width = window.innerWidth || document.body.clientWidth;
    this.height = window.innerHeight || document.body.clientHeight;
    this.midX = ~~(this.width / 2);
    this.midY = ~~(this.height / 2);

    this.app = new PIXI.Application({
      width: this.width,
      height: this.height,
      backgroundColor: COLORS.BLACK,
    });
    document.body.appendChild(this.app.view);

    /**
     * Create main scene
     */
    this.container = new PIXI.Container();
    this.app.stage.addChild(this.container);
    this.timeLine = new TimelineLite();
    /**
     * Absolutely dark screen to overflow other group
     */
    this.shutter = new PIXI.Graphics();

    /**
     * Add it to scene asap
     */
    this.shutter.beginFill(COLORS.BLACK, 1);
    this.shutter.drawRect(0, 0, this.width, this.height);
    this.shutter.endFill();
    this.background = new PIXI.Graphics();
    this.background.beginFill(COLORS.BLACK, 1);
    this.background.drawRect(0, 0, this.width, this.height);
    this.background.endFill();

    /**
     * Create text label
     * @type {.exports.Text}
     */
    this.text = new PIXI.Text('LOADING', { fontFamily: 'Knewave', fontSize: '50px', fill: COLORS.WHITE });
    this.text.position.x = 0;
    this.text.position.y = this.midY / 2;
    /**
     * Create rotating figure
     * @type {Sprite}
     */
    this.loadingFigure = PIXI.Sprite.from('game/assets/images/loading2.png');
    this.loadingFigure.alpha = 0;
    this.loadingFigure.width = 10;
    this.loadingFigure.height = 10;
    this.loadingFigure.position.x = this.midX - this.loadingFigure.width / 2;
    this.loadingFigure.position.y = this.midY;
    /**
     * Set anchor to rotate sprite by center point
     */
    this.loadingFigure.anchor.x = 0.5;
    this.loadingFigure.anchor.y = 0.5;
    /**
     * Add elements to scene
     */
    this.app.stage.addChild(this.shutter);
    this.container.addChild(this.background);
    this.container.addChild(this.loadingFigure);
    this.container.addChild(this.text);
  }

  update = () => {
    if (!this.app.stage) { return false; }
    this.loadingFigure.rotation += 0.2;
    this.app.render(this.app.stage);
    requestAnimationFrame(this.update);
  };

  show() {
    TweenLite.to(this.shutter, 1, { alpha: 0 });
    /**
     * Move text from left side to center
     */
    this.timeLine.to(this.text, 0.5, {
      alpha: 1,
      x: this.midX - this.text.width / 2,
      ease: Back.easeOut.config(2)
    });
    /**
     * Appear the circle part
     */
    this.timeLine.to(this.loadingFigure, 2, {
      alpha: 1,
      height: 80,
      width: 80,
      ease: Expo.easeOut
    });
    requestAnimationFrame(this.update.bind(this));
  }

  hide = () => new Promise((resolve) => {
    this.timeLine.to(this.loadingFigure, 1, { alpha: 0 });
    this.timeLine.to(this.text, 0.5, {
      x: 2000,
      ease: Back.easeIn.config(2)
    });
    this.timeLine.to(this.shutter, 0.5, {
      alpha: 1,
      onComplete: () => {
        resolve();
        /**
         * Delete canvas
         */
        document.body.removeChild(this.app.view);
        delete this.stage;
      }
    });
  });
}

export default LoadingScreen;
