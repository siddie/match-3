import LoadingController from './preloader/LoadingController';
import SceneManager from './main/SceneManager';
import event, { EVENTS } from 'utils/event';
import { GAME_MENU, GAME_SCENE } from 'dictionary/scenes';

class Application {
  constructor(options) {
    this.options = options;

    // Create loader instance for preload assets and show shutter
    const loadingController = new LoadingController();

    // Start preload resources and show the loading screen
    loadingController
      .load()
      .then(assets => this.start(assets));
  }

  start(assets) {
    this.sceneManager = new SceneManager(assets);

    event.on(EVENTS.START_NEW_GAME, () => this.sceneManager.showScene(GAME_SCENE));
    event.on(EVENTS.CONTINUE, () => this.sceneManager.showScene(GAME_SCENE));
    event.on(EVENTS.BACK_TO_MENU, () => this.sceneManager.showScene(GAME_MENU));
    this.sceneManager.showScene(GAME_MENU);
  }
}

export default Application;
