import * as PIXI from 'pixi.js';

const OFFSET = 20;
const TEXT_OPTIONS = {
  fontSize: '30px',
  fontFamily: 'Knewave',
  fill: 'white',
};

class Score {
  constructor(container) {
    this.score = 0;
    this.container = container;

    this.scoreText = new PIXI.Text(this.score, TEXT_OPTIONS);
  }

  setPosition(x, y) {
    this.scoreText.position.x = x;
    this.scoreText.position.y = y;
  }

  addValue(v) {
    this.score = this.score + v;
    this.scoreText.text = this.score;
  }

  reset() {
    this.score = 0;
    this.scoreText.text = this.score;
  }
}


export default Score;
