import { rand } from 'utils/common';

class Grid {
  constructor({ width, height, jewelTypes }) {
    this.width = width;
    this.height = height;
    this.types = jewelTypes;
    this.grid = [];
  }

  forEach() {
    return this.grid.forEach(...arguments);
  }

  getItem(x, y) {
    return this.grid[x][y];
  }

  removeItem(x, y) {
    this.grid[x][y] = null;
  }

  removeItems = items => items.forEach(({ x, y }) => this.removeItem(x, y));

  swap(p1, p2) {
    const temp = this.grid[p1.x][p1.y];
    this.grid[p1.x][p1.y] = this.grid[p2.x][p2.y];
    this.grid[p2.x][p2.y] = temp;
  }

  isNeighbours(j1, j2) {
    var coeff = Math.abs(j1.x - j2.x) + Math.abs(j1.y - j2.y);
    return (coeff === 1);
  }

  getWorldCoords(jewel) {
    for (let x = 0; x < this.grid.length; ++x) {
      const row = this.grid[x];
      const y = row.indexOf(jewel);
      if (y > -1) {
        return { x, y };
      }
    }
    return null;
  };

  findMathesFor(x, y) {
    if (!this.grid[x][y]) {
      console.warn('Call find matches to empty block');
      return [];
    }
    const { typeID } = this.grid[x][y];
    const checkedX = [{ x, y }];
    const checkedY = [{ x, y }];

    // <---
    for (let i = x - 1; i >= 0; i--) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({ x: i, y });
    }

    // --->
    for (let i = x + 1; i < this.grid.length; i++) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({ x: i, y });
    }

    // ↑
    for (let i = y - 1; i >= 0; i--) {
      const compareJewel = this.grid[x][i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({ x, y: i });
    }

    // ↓
    for (let i = y + 1; i < this.grid[x].length; i++) {
      const compareJewel = this.grid[x][i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({ x, y: i });
    }

    if (checkedX.length >= 3 && checkedY.length >= 3) {
      return checkedX.concat(checkedY);
    }

    if (checkedY.length >= 3) {
      return checkedY;
    }

    if (checkedX.length >= 3) {
      return checkedX;
    }

    return [];
  }

  checkRightPlacement(x, y, typeID, column) {
    const checkedX = [{ x, y }];
    const checkedY = [{ x, y }];

    // <---
    for (let i = x - 1; i >= Math.max(0, x - 2); i--) {
      const compareJewel = this.grid[i][y];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedX.push({ x: i, y });
    }


    // ↑
    for (let i = y - 1; i >= Math.max(0, y - 2); i--) {
      const compareJewel = column[i];

      if (!compareJewel || compareJewel.typeID !== typeID) {
        break;
      }
      checkedY.push({ x, y: i });
    }

    if (checkedX.length >= 3 && checkedY.length >= 3) {
      return checkedX.concat(checkedY);
    }

    if (checkedY.length >= 3) {
      return checkedY;
    }

    if (checkedX.length >= 3) {
      return checkedX;
    }

    return false;
  };

  checkAvailbleSwipes() {
    const checkVerticalMatching = (typeID, x, y, swipedJewel = {}) => {
      let count = 0;
      for (let i = y + 1; i < this.height; i++) {
        const jewelType = (swipedJewel.y === i) ? swipedJewel.typeID : this.grid[x][i].typeID;
        if (jewelType === typeID) {
          count++;
        } else {
          break;
        }
      }

      for (let i = y - 1; i >= 0; i--) {
        const jewelType = (swipedJewel.y === i) ? swipedJewel.typeID : this.grid[x][i].typeID;
        if (jewelType === typeID) {
          count++;
        } else {
          break;
        }
      }

      return count >= 2;
    }

    const checkHorizontalMatching = (typeID, x, y, swipedJewel = {}) => {
      let count = 0;
      for (let i = x + 1; i < this.width; i++) {
        const jewelType = (swipedJewel.x === i) ? swipedJewel.typeID : this.grid[i][y].typeID;
        if (jewelType === typeID) {
          count++;
        } else {
          break;
        }
      }

      for (let i = x - 1; i >= 0; i--) {
        const jewelType = (swipedJewel.x === i) ? swipedJewel.typeID : this.grid[i][y].typeID;
        if (jewelType === typeID) {
          count++;
        } else {
          break;
        }
      }

      return count >= 2;
    }


    const checkSwipesForJewels = (jewel, x, y) => {
      const result = [];
      const { typeID } = jewel;

      // Process right swiping
      if ((x + 1) < this.width) {
        const leftTypeID = this.grid[x + 1][y].typeID;
        if (checkVerticalMatching(leftTypeID, x, y) || checkHorizontalMatching(leftTypeID, x, y, { typeID, x: x + 1 })) {
          result.push({ from: { x: x + 1, y }, to: { x, y } });
        }

        if (checkVerticalMatching(typeID, x + 1, y) || checkHorizontalMatching(typeID, x + 1, y, { typeID: leftTypeID, x })) {
          result.push({ from: { x, y }, to: { x: x + 1, y } });
        }
      }

      // Process bottom swiping
      if ((y + 1) < this.height) {
        const leftTypeID = this.grid[x][y + 1].typeID;

        // Check left bottom
        if (checkVerticalMatching(leftTypeID, x, y, { typeID, y: y + 1 }) || checkHorizontalMatching(leftTypeID, x, y)) {
          result.push({ from: { x, y: y + 1 }, to: { x, y } });
        }
        if (checkVerticalMatching(typeID, x, y + 1, { typeID: leftTypeID, y }) || checkHorizontalMatching(typeID, x, y + 1)) {
          result.push({ from: { x, y }, to: { x, y: y + 1 } });
        }
      }

      return result;
    };

    const matches = [];
    this.grid.forEach((column, x) => {
      column.forEach((item, y) => {
        const match = checkSwipesForJewels(item, x, y);
        if (match.length > 0) {
          matches.push(match);
        }
      });
    });

    return matches.flat();
  }

  generate(drawCallback) {
    for (let x = 0; x < this.width; x++) {
      const column = [];
      for (let y = 0; y < this.height; y++) {
        let typeID = rand(this.types);
        let attempts = 0;
        do {
          const needToDrop = this.checkRightPlacement(x, y, typeID, column);
          if (!needToDrop) {
            break;
          }
          attempts++;
          typeID = rand(this.types);
        } while (attempts < 100);

        const jewel = drawCallback(typeID, x, y);
        column.push(jewel);
      }
      this.grid.push(column);
    }
  }

  refillColumn = (x, createJewel) => new Promise(async (resolve) => {
    const column = this.grid[x];
    const hasEmptyValues = this.grid[x].some(v => v === null);
    const recheckPoints = [];

    if (!hasEmptyValues) {
      return resolve(recheckPoints);
    }

    // Add recheck poinst from latest null index to 0
    let latestNullPosition = column.lastIndexOf(null);
    for (let y = latestNullPosition; y >= 0; y--) {
      recheckPoints.push({ x, y });
    }

    let moveDown = false;
    let count = 0;

    const jewelAnimPromises = [];
    for (let y = column.length - 1; y >= 0; y--){
      const jewel = column[y];

      if (jewel && moveDown) {
        this.grid[x][y] = null;
        this.grid[x][y + count] = jewel;
        jewelAnimPromises.push(jewel.moveDownTo(count));
      }

      if (!jewel) {
        moveDown = true;
        count++;
      }
    }

    const newJewelsPromisesStack = [];
    for (let i = count - 1; i >= 0; i--) {
      column[i] = createJewel(
        rand(this.types),
        x,
        (count - i) * -1,
      );
      newJewelsPromisesStack.push(column[i].moveDownTo(count));
    }

    await Promise.all(newJewelsPromisesStack.concat(jewelAnimPromises));

    resolve(recheckPoints);
  });

  refillColumns({ createJewel }) {
    return this.grid.map((column, x) => this.refillColumn(x, createJewel));
  }
}

export default Grid;