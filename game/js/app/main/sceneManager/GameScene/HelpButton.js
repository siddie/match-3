import * as PIXI from 'pixi.js';
import Button from 'main/accessory/Button';

const btnTexture = new PIXI.Texture.from('game/assets/images/gear_btn.png');
const btnTexturePressed = new PIXI.Texture.from('game/assets/images/gear_btn_pressed.png');

const BUTTON_HEIGHT = 50;
const BUTTON_WIDTH = 50;

class HelpButton extends Button {
  constructor(x, y) {
    super(x, y);

    this.texture = new PIXI.Sprite(btnTexture);
    this.texture.buttonMode = true;
    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;

    this.graphics.addChild(this.texture);
  }

  onMouseUp(e) {
    if (!this.isDown) return;
    this.texture.texture = btnTexture;

    super.onMouseUp(e);
  };

  onMouseDown(e) {
    this.texture.texture = btnTexturePressed;

    super.onMouseDown(e);
  };
}

export default HelpButton;
