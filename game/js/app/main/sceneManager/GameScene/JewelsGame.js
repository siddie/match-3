import * as PIXI from 'pixi.js';
import event, { EVENTS } from 'utils/event';
import Jewel from './Jewel';
import Grid from './Grid';

class JewelsGame {
  constructor(options) {
    this.options = options;
    this.container = options.container;
    this.width = options.width;
    this.height = options.height;

    this.jewelsContainer = new PIXI.Container();
    this.debugContainer = new PIXI.Container();
    this.debugContainer.visible = false;

    this.container.addChild(this.jewelsContainer);
    this.container.addChild(this.debugContainer);

    this.grid = new Grid(options);
    this.grid.generate(this.createJewel);

    const matches = this.grid.checkAvailbleSwipes();
    this.drawMatches(matches);

    this._firstJewel = null;
    this._secondJewel = null;
    this._seeking = false;
    this._disableInput = false;
  }

  toggleDebug() {
    this.debugContainer.visible = !this.debugContainer.visible;
  }

  drawMatches(matches) {
    const {
      jewelSize,
      jewelOffset,
    } = this.options;

    this.debugContainer.removeChildren();

    matches.forEach(({ from, to }) => {
      const line = new PIXI.Graphics();
      line.lineStyle(4, 0x000000, 1);
      line.moveTo(
        from.x * jewelSize + from.x * jewelOffset + jewelSize / 2,
        from.y * jewelSize + from.y * jewelOffset + jewelSize / 2,
      );
      line.lineTo(
        to.x * jewelSize + to.x * jewelOffset + jewelSize / 2,
        to.y * jewelSize + to.y * jewelOffset + jewelSize / 2,
      );
      this.debugContainer.addChild(line);
    });
  }

  destroy() {
    this.container.removeChildren();
  }

  animateDrop() {
    const { jewelSize, jewelOffset } = this.options;
    const masterTimeline = new TimelineLite();

    this.grid.forEach((column, x) => {
      const timeline = new TimelineLite();

      column.forEach((jewel, index) => {
        if (jewel.type === -1) {
          return false;
        }
        jewel.sprite.y = -((column.length - index) * jewelSize + (column.length - index) * jewelOffset) - 100;
        timeline.to(jewel.sprite, 0.6, {
          y: index * jewelSize + index * jewelOffset,
          ease: Power0.easeNone,
        }, 0);
      });

      masterTimeline.add(timeline, x * 0.1);
    });
  }

  createJewel = (typeID, x, y) => {
    const { jewelSize, jewelOffset } = this.options;

    let jewel = new Jewel(
      typeID,
      x * jewelSize + x * jewelOffset,
      y * jewelSize + y * jewelOffset,
      jewelSize,
      jewelSize,
    );

    const proxyDown = this.jewelMouseDown.bind(this, jewel);
    const proxyUp = this.jewelMouseUp.bind(this, jewel);
    const proxyIn = this.jewelMouseIn.bind(this, jewel);
    const proxyOut = this.jewelMouseOut.bind(this, jewel);

    jewel.sprite.on("pointerdown", proxyDown);
    // jewel.sprite.on("pointerover", proxyIn);
    // jewel.sprite.on("pointerout", proxyOut);
    jewel.sprite.on("pointerup", proxyUp);

    jewel.sprite.on("touchend", proxyDown);

    this.jewelsContainer.addChild(jewel.sprite)
    return jewel;
  };

  jewelMouseDown(jewel) {
    if (this._disableInput) { return false; }
    if (!this._firstJewel || this._firstJewel === jewel) {
      this._firstJewel = jewel;
      this._seeking = true;
    } else {
      this._secondJewel = jewel;
      this.checkSelected();
    }
  };

  jewelMouseUp(jewel) {
    this._seeking = false;
  };

  jewelMouseOut(jewel, event) {
    if (!this._firstJewel || this._disableInput ) { return false; }
    this._seeking = true;
  };

  jewelMouseIn(jewel) {
    if (!this._seeking || this._disableInput) { return false; }

    this._secondJewel = jewel;
    this._seeking = false;
    this.checkSelected();
  };

  checkSelected() {
    var jFCoords = null;
    var jSCoords = null;

    var grid = this.grid;
    var gamePanel = this.container;
    var self = this;

    const isNeighbours = (j1, j2) => {
      jFCoords = this.grid.getWorldCoords(j1);
      jSCoords = this.grid.getWorldCoords(j2);
      return this.grid.isNeighbours(jFCoords, jSCoords);
    };

    var enableInput = function () {
      self._disableInput = false;
    };

    /**
     * Disable user input
     */
    this._disableInput = true;

    /**
     * If we have no target
     */
    if (!this._firstJewel || !this._secondJewel) {
      return false;
    }

    /**
     * If jewels of user choice are not neighbours
     */
    if (!isNeighbours(this._firstJewel, this._secondJewel)) {
      /**
       * Make second jewel are first and null second
       */
      this._firstJewel = this._secondJewel;
      this._secondJewel = null;

      /**
       * Enable input
       */
      this._disableInput = false;
      return;
    }

    /**
     * Swap jewels in grid data
     */
    this.grid.swap(jFCoords, jSCoords);
    this._firstJewel = this.grid.getItem(jFCoords.x, jFCoords.y);
    this._secondJewel = this.grid.getItem(jSCoords.x, jSCoords.y);

    /**
     * Function on end of swap animation
     */
    const onCompleteSwapping = () => {
      var itemsToRemove = this.grid.findMathesFor(jFCoords.x, jFCoords.y).concat(this.grid.findMathesFor(jSCoords.x, jSCoords.y))

      /**
       * If nothing to remove swap it again
       */
      if (itemsToRemove.length === 0) {
        this.grid.swap(jFCoords, jSCoords);
        return timeline.reverse();
      } else {
        /**
         * Remove all jewels consist of array from screen
         * @private
         */
        itemsToRemove.forEach((v, i) => {
          var jewel = this.grid.getItem(v.x, v.y);

          if (i < itemsToRemove.length - 1) {
            /**
             * Play animation moving jewel to top center
             */
            jewel.moveTo(gamePanel.width / 2, -100);
          } else {
            /**
             * Play animation moving jewel to top center
             */
            jewel.moveTo(gamePanel.width / 2, -100, () => {
              self.finalizeChecking(itemsToRemove.length);
              event.trigger(EVENTS.ADD_SCORE, {
                counter: 1,
                jewels: itemsToRemove.length,
              });
            });
          }
        });

        this.grid.removeItems(itemsToRemove);
      }
      this._firstJewel = this._secondJewel = null;
    };

    /**
     * Create moving animation
     */
    const timeline = new TimelineLite({ onReverseComplete: enableInput });
    timeline.insert(TweenLite.to(this._firstJewel.sprite, 0.3, {
      x: self._secondJewel.sprite.x,
      y: self._secondJewel.sprite.y,
      ease: Power2.easeIn
    }));
    timeline.insert(TweenLite.to(this._secondJewel.sprite, 0.3, {
      x: self._firstJewel.sprite.x,
      y: self._firstJewel.sprite.y,
      ease: Power2.easeIn,
      onComplete: onCompleteSwapping
    }));
  };

  checkSingleJewel = ({ x, y }) => new Promise(async (resolve) => {
    var itemsToRemove = this.grid.findMathesFor(x, y);

    if (itemsToRemove.length === 0) {
      return resolve(0);
    }
    // Force pause before remove jewels going down
    if (itemsToRemove.length > 0) {
      await new Promise(resolve => setTimeout(resolve, 300))
    }

    let jewelsTriggered = 0;
    const animations = itemsToRemove.map(({ x, y }) => new Promise((animationComplete) => {
      const jewel = this.grid.getItem(x, y);
      if (jewel) {
        jewelsTriggered += 1;
        jewel.moveTo(this.container.width / 2, -100, animationComplete);
      } else {
        animationComplete()
      }
      this.grid.removeItem(x, y);
    }));
    await Promise.all(animations);

    resolve(jewelsTriggered);
  });

  finalizeChecking = async (jewels) => {
    const promises = this.grid.refillColumns({
      createJewel: this.createJewel,
    });
    const data = await Promise.all(promises);
    let itemsToCheck = data.flat();

    let counter = 0;
    event.trigger(EVENTS.ADD_SCORE, {
      counter,
      jewels,
    });

    do {
      counter += 1;
      const count = await Promise.all(itemsToCheck.map(item => this.checkSingleJewel(item)));
      await new Promise(resolve => setTimeout(resolve, 300))
      const promises = this.grid.refillColumns({
        createJewel: this.createJewel,
      });
      const items2 = await Promise.all(promises);
      const matches = this.grid.checkAvailbleSwipes();
      this.drawMatches(matches);

      itemsToCheck = items2.filter(v => v.length > 0).flat();
      event.trigger(EVENTS.ADD_SCORE, {
        counter,
        jewels: count.reduce((acc, v) => acc + v),
      });
    } while (itemsToCheck.length > 0);

    this._disableInput = false;
  };
}

export default JewelsGame;
