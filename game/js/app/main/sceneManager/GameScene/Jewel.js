import * as PIXI from 'pixi.js';
import { TweenLite } from 'gsap';

class Jewel {
  constructor(id, x, y, w, h) {
    this.typeID = id;

    this.sprite = new PIXI.Sprite.from(`game/assets/images/jewels/jewel${id}.png`);
    this.sprite.width = w;
    this.sprite.height = h;
    this.sprite.position.x = x;
    this.sprite.position.y = y;

    this.sprite.interactive = true;
    this.sprite.on('pointerover', this.onMouseOver.bind(this));
    this.sprite.on('pointerout', this.onMouseOut.bind(this));
    this.sprite.on('pointerdown', this.onMouseDown.bind(this));
    this.sprite.on('pointerup', this.onMouseUp.bind(this));

    this.isMouseDown = true;
    this.timeline = new TimelineLite();
  }

  onMouseOver(e) { };

  onMouseOut(e) {
    if (this.isMouseDown) {
      this.onMouseUp(e);
    }
  };

  onMouseDown() {
    this.isMouseDown = true;
    this.sprite.tint = 0xbbbbbb;
  };

  onMouseUp() {
    this.isMouseDown = false;
    this.sprite.tint = 0xffffff;
  };

  moveTo(x, y, onComplete, time = 0.5) {
    this.sprite.parent.setChildIndex(this.sprite, this.sprite.parent.children.length - 1);
    this.timeline.add(
      TweenLite.to(this.sprite, time, {
        x,
        y,
        // alpha: 0,
        ease: Power0.easeNone,
        onComplete,
      })
    );
  };

  // flyAway(x, y, onComplete, time = 0.5) {
  //   // Bring sprite to front
  //   this.sprite.parent.setChildIndex(this.sprite, this.sprite.parent.children.length - 1);

  //   let curve = [
  //     {
  //       x: this.sprite.x + 200,
  //       y: this.sprite.y - 150,
  //     },
  //     {
  //       x: this.sprite.x + 600,
  //       y: this.sprite.y + 600,
  //     },
  //   ]

  //   TweenMax.to(this.sprite, 2, {
  //     bezier: {
  //       type: 'thru',
  //       curviness: 1,
  //       timeResolution: 6,
  //       values: curve,
  //       autoRotate: false,
  //     },
  //     ease: Power1.easeIn,
  //     onComplete,
  //   });
  //   console.log(this.sprite);
    
  //   // debugger;
  //   const scale = this.sprite.scale.x;
  //   TweenLite.to(this.sprite, 2, {
  //     // scale: (v, i) => {
  //     //   console.log(v, i)
  //     // },
  //     // alpha: 0,
  //     onUpdate(v ,i) {
  //       console.log(this.ratio);
  //       this.target.scale.set(scale + this.ratio * scale * 0.5, scale + this.ratio * scale * 0.5)
  //       // console.log(v, i);
  //     },
  //     ease: Power0.easeNone,
  //   })
  // };

  moveDown(onComplete) {
    const tween = TweenLite.to(this.sprite, 0.1, {
      y: `+=${this.sprite.height + 5}`,
      ease: Power0.easeNone,
      onComplete,
    });
    this.timeline.add(tween);
  }

  moveDownTo = (times = 1, onComplete) => new Promise((resolve) => {
    const tween = TweenLite.to(this.sprite, 0.1 * times, {
      y: `+=${(this.sprite.height + 5) * times}`,
      ease: Power0.easeNone,
      onComplete: () => {
        onComplete && onComplete();
        resolve();
      },
    });
    this.timeline.add(tween);
  });
}

export default Jewel;
