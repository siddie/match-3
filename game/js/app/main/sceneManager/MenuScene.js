import * as PIXI from 'pixi.js';
import { TweenLite } from 'gsap';
import EmptyScene from './EmptyScene';
import MenuButton from 'main/sceneManager/MenuScene/MenuButton';
import WindEmmiter from './MenuScene/WindEmmiter';
import Cloud from './MenuScene/Cloud';
import event, { EVENTS } from 'utils/event';
import { rand } from 'utils/common';
import COLORS from 'utils/colors';

function updateSizeAndPosition(sprite, width, height) {
  sprite.height = height;
  sprite.width = sprite.height / 0.75;
  sprite.x = width / 2;
}

class MenusScene extends EmptyScene {
  constructor(assets, width, height) {
    super(width, height);
    this.assets = assets;
    this.width = width;
    this.height = height;

    /**
     * Create container for this scene
     * @type {Container}
     */
    this.screen = new PIXI.Container();
    const { screen } = this;
    /**
     * Put it before shutter
     */
    this.container.addChildAt(screen, 0);
    this.buttons = [];
    this.clouds = [];
    this.static = {};
    /**
     * Create background image
     */
    this.static.background = this.createStaticItem('START_MENU_BACKGROUND');

    const particlesContainer = new PIXI.Container();
    screen.addChild(particlesContainer)

    this.windEmmiter = new WindEmmiter(particlesContainer, assets.WIND_PARTICLE.texture);
    this.createClouds();

    this.static.backTrees = this.createStaticItem('START_MENU_TREE');
    this.static.frontTrees = this.createStaticItem('START_MENU_TREES');
    this.static.ground = this.createStaticItem('START_MENU_GROUND');

    this.shutters = {
      borderLeft: new PIXI.Graphics(),
      borderRight: new PIXI.Graphics(),
    }
    screen.addChild(this.shutters.borderLeft);
    screen.addChild(this.shutters.borderRight);
    this.redrawShutterBorder();

    /**
     * Define menu buttons data
     */
    const BUTTONS_DATA = [
      {
        text: 'Continue',
        visible: false,
        onClick: function () {
          event.trigger(EVENTS.CONTINUE);
        },
      },
      {
        text: 'New Game',
        visible: true,
        onClick: function () {
          event.trigger(EVENTS.START_NEW_GAME);
        },
      },
      {
        text: 'About',
        visible: true,
        onClick: () => {
          TweenLite.to(this.buttons[0].graphics, 1, { y: "-=500", ease: Power2.easeIn });
          TweenLite.to(this.buttons[1].graphics, 1.2, { y: "-=700", ease: Power2.easeIn });
          TweenLite.to(this.buttons[2].graphics, 1.5, { y: "-=900", ease: Power2.easeIn });
        },
      }
    ];
    event.on(EVENTS.START_NEW_GAME, () => {
      setTimeout(() => {
        this.buttons[0].graphics.visible = true;
      }, 500);
    });

    /**
     * Create menu buttons
     */
    for (let i = 0; i < BUTTONS_DATA.length; ++i) {
      const button = new MenuButton({
        label: BUTTONS_DATA[i].text,
        visible: BUTTONS_DATA[i].visible,
        orderNumber: i,
        width,
        height,
        assets,
      });

      button.onClick = BUTTONS_DATA[i].onClick;
      screen.addChild(button.graphics);
      this.buttons.push(button);
    }

    this.elapsed = new Date();
  }

  createStaticItem(image) {
    const sprite = new PIXI.Sprite.from(image);
    sprite.anchor.set(0.5, 0);
    updateSizeAndPosition(sprite, this.width, this.height);
    this.screen.addChild(sprite);

    return sprite;
  }

  createClouds() {
    const CLOUD_TYPES = 5;

    for (let i = 0; i < 5; i++) {
      const cloud = new Cloud(this.assets[`CLOUD_${rand(CLOUD_TYPES)}`].texture, this.width, this.height);
      this.screen.addChild(cloud);
      this.clouds.push(cloud);
    }
  }

  update() {
    const now = new Date();

    this.windEmmiter.update((now - this.elapsed));
    this.clouds.forEach(({ update }) => update());

    this.elapsed = now;
  }

  redrawShutterBorder() {
    const { width: bgWidth } = this.static.background;
    const { borderLeft, borderRight } = this.shutters;

    if (this.width <= bgWidth) {
      borderLeft.clear();
      borderRight.clear();
      return false;
    }

    const delta = this.width - bgWidth;
    borderLeft.clear();
    borderLeft.beginFill(COLORS.BLACK);
    borderLeft.drawRect(0, 0, delta / 2, this.height);
    borderLeft.endFill();

    borderRight.clear();
    borderRight.beginFill(COLORS.BLACK);
    borderRight.drawRect(delta / 2 + bgWidth, 0, delta / 2, this.height);
    borderRight.endFill();
  }

  resize(width, height) {
    this.width = width;
    this.height = height;

    this.buttons.forEach((btn, index) => btn.resize(width, height, index));
    for (let key in this.static) {
      updateSizeAndPosition(this.static[key], width, height);
    }

    this.redrawShutterBorder();
  }
}

export default MenusScene;
