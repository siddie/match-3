import * as PIXI from 'pixi.js';
import * as particles from 'pixi-particles';

const options = {
  "alpha": {
    "start": 0.9,
    "end": 0.44
  },
  "scale": {
    "start": 0.5,
    "end": 0.5,
    "minimumScaleMultiplier": 0.5
  },
  "color": {
    "start": "#ffffff",
    "end": "#ffffff"
  },
  "speed": {
    "start": 200,
    "end": 200,
    "minimumSpeedMultiplier": 0.99
  },
  "acceleration": {
    "x": 0,
    "y": 2
  },
  "maxSpeed": 0,
  "startRotation": {
    "min": 0,
    "max": 0
  },
  "noRotation": false,
  "rotationSpeed": {
    "min": 0,
    "max": 0
  },
  "lifetime": {
    "min": 5,
    "max": 10
  },
  "blendMode": "normal",
  "frequency": 0.5,
  "emitterLifetime": -1,
  "maxParticles": 500,
  "pos": {
    "x": 0,
    "y": 0
  },
  "addAtBack": false,
  "spawnType": "rect",
  "spawnRect": {
    "x": -100,
    "y": 0,
    "w": 100,
    "h": 400
  }
};

class WindEmmiter {
  constructor(container, texture) {
    this.emitter = new particles.Emitter(
      container,
      [texture],
      options
    );

    this.emitter.emit = true;
  }

  update(delta) {
    this.emitter.update(delta * 0.001);
  }
}

export default WindEmmiter;
