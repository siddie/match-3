import * as PIXI from 'pixi.js';
import { between, rand } from 'utils/common';

class Cloud extends PIXI.Sprite {
  constructor(texture, width, height) {
    super(texture);

    this.screenWidth = width;
    this.screenHeight = height;

    this.y = between(-20, this.screenHeight / 3);
    this.x = rand(width);
    this.speed = between(0.2, 3);
  }

  setBounds(width, height) {
    this.screenWidth = width;
    this.screenHeight = height;

    this.y = between(-20, this.screenHeight / 3);
  }

  update = () => {
    this.x += this.speed;
    if (this.x > this.screenWidth) {
      this.x = -200;
      this.speed = between(0.2, 3);
      this.y = between(-20, 300);
    }
  }
}

export default Cloud;
