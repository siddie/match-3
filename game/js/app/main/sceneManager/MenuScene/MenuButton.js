import * as PIXI from 'pixi.js';
import Button from 'main/accessory/Button';

const OFFSET = 30;
const BUTTON_WIDTH = 200;
const BUTTON_HEIGHT = BUTTON_WIDTH / 2.24;
const TEXT_OPTIONS = {
  fontFamily: 'Knewave',
  fill: '#3f3643',
  dropShadow: true,
  dropShadowColor: '#bababa',
  dropShadowDistance: 2,
  letterSpacing: 1
};

class MenuButton extends Button {
  constructor({ width, height, visible, orderNumber, label, assets }) { 
    const x = width / 2 - BUTTON_WIDTH / 2;
    const y = height * 0.1 + (BUTTON_HEIGHT + OFFSET) * orderNumber;
    super(x, y);

    this.buttonTexture = assets.MENU_BUTTON.texture;
    this.buttonTexturePressed = assets.MENU_BUTTON_PRESSED.texture;

    this.texture = new PIXI.Sprite(this.buttonTexture);
    this.texture.buttonMode = true;
    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;

    this.graphics.visible = visible;
    this.graphics.addChild(this.texture);

    this.text = new PIXI.Text(label, TEXT_OPTIONS);
    this.text.anchor.set(0.5, 0.5);
    this.text.position.x = this.graphics.width / 2;
    this.text.position.y = this.graphics.height / 2.5;

    this.graphics.addChild(this.text);
  }

  onMouseUp(e) {
    if (!this.isDown) return;
    this.text.position.y -= 4;
    this.texture.texture = this.buttonTexture;

    super.onMouseUp(e);
  };

  onMouseDown(e) {
    this.text.position.y += 4;
    this.texture.texture = this.buttonTexturePressed;

    super.onMouseDown(e);
  };

  resize(width, height, order) {
    const x = width / 2 - BUTTON_WIDTH / 2;
    const y = height * 0.1 + (BUTTON_HEIGHT + OFFSET) * order;

    this.texture.width = BUTTON_WIDTH;
    this.texture.height = BUTTON_HEIGHT;
    this.graphics.position.x = x;
    this.graphics.position.y = y;
  }
}

export default MenuButton;
