import * as PIXI from 'pixi.js';
import event, { EVENTS } from 'utils/event';
import COLORS from 'utils/colors';
import EmptyScene from './EmptyScene';
import JewelsGame from './GameScene/JewelsGame';
import HomeButton from './GameScene/HomeButton';
import HelpButton from './GameScene/HelpButton';
import Score from './GameScene/Score';

const DEFAULT_GAME_MODEL = {
  width: 8,
  height: 8,
  jewelTypes: 3,
  jewelOffset: 5,
}

class GameScene extends EmptyScene {
  constructor(assets, width, height, options) {
    super(width, height);
    this.width = width;
    this.height = height;

    this.gameOptions = {
      ...DEFAULT_GAME_MODEL,
      ...options,
      jewelSize: Math.min(this.width / 10, 50), 
    };

    // Put it before shutter
    this.screen = new PIXI.Container();
    this.container.addChildAt(this.screen, 0);

    this.backgroundImage = new PIXI.Sprite.from(assets.GAME_BACKGROUND.texture);
    const backgroundHeight = Math.max(height, 600);
    this.backgroundImage.height = backgroundHeight;
    this.backgroundImage.width = backgroundHeight * 1.4;
    this.backgroundImage.anchor.set(0.5, 0);
    this.backgroundImage.x = width / 2;
    this.screen.addChild(this.backgroundImage);
    
    this.homeBtn = new HomeButton(0, 10);
    this.homeBtn.onClick = () => event.trigger(EVENTS.BACK_TO_MENU);

    this.helpBtn = new HelpButton(0, 10);
    this.helpBtn.onClick = () => this.game.toggleDebug();
    
    this.score = new Score(this.screen);
    this.setupGameScreen();  

    this.screen.addChild(this.homeBtn.graphics);
    this.screen.addChild(this.helpBtn.graphics);
    this.screen.addChild(this.score.scoreText);

    event.on(EVENTS.START_NEW_GAME, this.resetGame);
    event.on(EVENTS.ADD_SCORE, ({ counter, jewels }) => {
      this.score.addValue(counter * jewels);
    })
  };

  setupGameScreen() {
    /* main game container */
    this.gameArea = new PIXI.Container();
    this.gameOptions.container = this.gameArea;

    /* background */
    this.background = new PIXI.Graphics();
    this.screen.addChild(this.background);
    this.screen.addChild(this.gameArea);

    /* draw top tplate */
    this.topPlate = new PIXI.Graphics();
    this.screen.addChild(this.topPlate);

    this.updateGameScreen(this.width, this.height);
  }

  updateGameScreen(w, h) {
    const {
      jewelSize,
      jewelOffset,
      width,
      height,
    } = this.gameOptions;

    this.gameOptions.jewelSize = Math.min(w / 10, 50);

    const gameAreaWidth = jewelSize * width + jewelOffset * (width - 1);
    const gameAreaHeight = jewelSize * height + jewelOffset * (height - 1);
    const backgroundWidth = w / 2 - gameAreaWidth / 2 - jewelSize / 2;
    
    this.homeBtn.setPosition(backgroundWidth + gameAreaWidth - 30, 10);
    this.helpBtn.setPosition(backgroundWidth + gameAreaWidth - 90, 10);
    this.score.setPosition(backgroundWidth + 30, 20);

    this.background.clear();
    this.background.beginFill(COLORS.JAGGER, 0.8);
    this.background.drawRect(
      backgroundWidth,
      100 - jewelSize / 2,
      gameAreaWidth + jewelSize,
      gameAreaHeight + jewelSize,
    );
    this.background.endFill();
    
    this.gameArea.width = gameAreaWidth;
    this.gameArea.height = gameAreaHeight;
    this.gameArea.position.x = w / 2 - gameAreaWidth / 2;
    this.gameArea.position.y = 100;

    /* draw top tplate */
    this.topPlate.clear();
    this.topPlate.beginFill(COLORS.JAGGER, 1);
    this.topPlate.drawRect(
      backgroundWidth,
      0,
      gameAreaWidth + jewelSize,
      100 - jewelSize / 2,
    );
    this.topPlate.endFill();
  }

  resetGame = () => {
    if (this.game) {
      this.game.destroy();
    }
    this.game = new JewelsGame(this.gameOptions);
    this.score.reset();
  }

  show(showTime) {
    super.show(showTime);
    this.game.animateDrop();
  };

  resize(width, height) {
    const backgroundHeight = Math.max(height, 600);
    const backgroundWidth = height * 1.4;
    this.backgroundImage.height = backgroundHeight;
    this.backgroundImage.width = backgroundWidth;
    this.backgroundImage.x = width / 2;

    this.updateGameScreen(width, height);
    // this.score.setPosition(width / 2 - backgroundWidth / 2);
    // this.homeBtn.setPosition(width / 2 + backgroundWidth / 2 - 60, 10);
    // this.helpBtn.setPosition(width / 2 + backgroundWidth / 2 - 120, 10);
  }
}

export default GameScene;
