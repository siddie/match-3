import * as PIXI from 'pixi.js';
import COLORS from 'utils/colors';

class EmptyScene {
 constructor( width, height ) {
    this.container = new PIXI.Container();

    // Absolutely dark screen to overflow other group
    this.shutter = new PIXI.Graphics();
    this.shutter.beginFill(COLORS.BLACK, 1);
    this.shutter.drawRect(0, 0, width, height);
    this.shutter.endFill();

    this.container.addChild( this.shutter );
  };

  show(showTime = 0.5) {
    return new Promise((onComplete) => {
      // Hide shutter by alpha animation
      TweenLite.to(this.shutter, showTime, { alpha: 0, onComplete });
    });
  }

  hide = () => new Promise((onComplete) => {
    TweenLite.to(this.shutter, 0.25, {
      alpha: 1,
      onComplete,
    });
  });

  resize() {}
}

export default EmptyScene;
