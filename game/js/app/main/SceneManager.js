import * as PIXI from 'pixi.js';
import { GAME_MENU, GAME_SCENE } from 'dictionary/scenes';
import MenuScene from './sceneManager/MenuScene';
import GameScene from './sceneManager/GameScene';
import COLORS from 'utils/colors';

const SCENES_MAP = {
  [GAME_MENU]: MenuScene,
  [GAME_SCENE]: GameScene,
};

class SceneManager {
  constructor(assets) {
    // Create container for holding all scenes
    this.scenes = {};
    this.currenScene = null;
    this.prevScene = null;

    this.width = window.innerWidth || document.body.clientWidth;
    this.height = window.innerHeight || document.body.clientHeight;

    // Create main canvas
    this.app = new PIXI.Application({
      width: this.width,
      height: this.height,
      backgroundColor: COLORS.BLACK,
      resizeTo: window,
    });

    document.body.appendChild(this.app.view);

    Object.keys(SCENES_MAP).map((key) => {
      this.scenes[key] = new SCENES_MAP[key](assets, this.width, this.height)
    });

    window.addEventListener('resize', this.resize);
    window.onorientationchange = this.resize;
    requestAnimationFrame(this.update);
  }


  update = () => {
    this.app.render(this.app.stage);
    if (this.currenScene.update) {
      try {
        this.currenScene.update();
      } catch (e) {
        console.log('Error during scene update');
        console.log(e);
      }
    }
    requestAnimationFrame(this.update);
  };

  changeScenes() {
    this.app.stage.removeChild(this.prevScene.container);
    this.app.stage.addChild(this.currenScene.container);
    this.currenScene.show();
  };

  showScene(key) {
    const scene = this.scenes[key];

    // If its a first scene in the game
    if (!this.currenScene) {
      this.app.stage.addChild(scene.container);
      scene.show(GAME_MENU);
    } else {
      this.currenScene
        .hide()
        .then(() => { this.changeScenes() });
      this.prevScene = this.currenScene;
    }
    this.currenScene = scene;
  };

  resize = () => {
    window.scrollTo(0, 0);
    const width = window.innerWidth || document.body.clientWidth;
    const height = window.innerHeight || document.body.clientHeight;

    const { view } = this.app;
    view.style.height = `${height}px`;
    view.style.width = `${width}px`;

    Object.keys(this.scenes).forEach(key => this.scenes[key].resize(width, height));
  };
}

export default SceneManager;
