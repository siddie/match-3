import * as PIXI from 'pixi.js';
import COLORS from 'UTILS/colors';

const getColor = function( id ){
  const listColors = [COLORS.PURPLE, COLORS.BLUE, COLORS.RED, COLORS.GREEN];

  return listColors[ id ];
};


const Jewel = function ( id, x, y, w, h ) {
  this.typeID = id;

  this.sprite = new PIXI.Graphics();
  this.sprite.beginFill( getColor(id), 1 );
  this.sprite.drawRect( 0, 0, w, h );
  this.sprite.endFill();
  this.sprite.position.x = x;
  this.sprite.position.y = y;
  // this.sprite.anchor.x = 0.5;
  // this.sprite.anchor.y = 0.5;

  this.sprite.interactive = true;
  this.sprite.on( "pointerover", this.onMouseOver.bind(this) );
  this.sprite.on( "pointerout", this.onMouseOut.bind(this) );
  this.sprite.on( "pointerdown", this.onMouseDown.bind(this) );
  this.sprite.on( "pointerup", this.onMouseUp.bind(this) );

  this.border = new PIXI.Graphics();
  this.border.beginFill( 0x00ff00 );
  this.border.drawRect( 0, 0, this.sprite.width, this.sprite.height );
  this.border.endFill();

  this.test = new PIXI.Graphics();
  this.test.beginFill( 0x00ffff );
  this.test.drawRect( 0, 0, w, h );
  this.test.endFill();

  this.isMouseDown = true;
  this.t = new TimelineLite();
};

Jewel.prototype.onMouseOver = function ( e ) {};

Jewel.prototype.onMouseOut = function ( e ) {
  if ( this.isMouseDown ) {
    this.onMouseUp( e );
  }
};

Jewel.prototype.onMouseDown = function () {
  this.isMouseDown = true;
  //this.sprite.addChild( this.border );
  this.sprite.tint = 0xbbbbbb;
};

Jewel.prototype.onMouseUp = function () {
  this.isMouseDown = false;
  this.sprite.tint = 0xffffff;
};

Jewel.prototype.moveTo = function ( x, y, cb ) {
  TweenLite.to( this.sprite, 0.5, {
    x: x,
    y: y,
    alpha: 0,
    ease: Power2.easeIn,
    onComplete: cb
  } );
};

Jewel.prototype._TEST = function(){
  this.sprite.addChild( this.test );
};

Jewel.prototype.X = function ( v ) {
  ( v ) && ( this.sprite.position.x = v );
  return this.sprite.position.x;
};

Jewel.prototype.Y = function ( v ) {
  ( v ) && ( this.sprite.position.y = v );
  return this.sprite.position.y;
};

export default Jewel;
