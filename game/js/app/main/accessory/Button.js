import * as PIXI from 'pixi.js';

class Button {
  constructor(x, y) {
    this.isDown = false;
    this.isOver = false;

    this.graphics = new PIXI.Container();
    this.graphics.interactive = true;

    this.graphics.position.x = x;
    this.graphics.position.y = y;

    this.graphics.on("pointerover", () => { this.onSpriteOver(); });
    this.graphics.on("pointerout", () => { this.onSpriteOut(); });
    this.graphics.on("pointerdown", () => { this.onMouseDown(); });
    this.graphics.on("pointerup", () => { this.onMouseUp(); });
  }

  onMouseDown() {
    this.isDown = true;
    this.onClick();
  };

  onMouseUp() {
    this.isDown = false;
  };

  onSpriteOver() {
    this.isOver = true;
  };

  setPosition(x, y) {
    this.graphics.x = x;
    this.graphics.y = y;
  }

  onSpriteOut() {
    this.isOver = false;
    if (this.isDown) {
      this.graphics.emit("pointerup");
      this.isDown = false;
    }
  };

  onClick() { };
}

export default Button;