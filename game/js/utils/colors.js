export default {
  BLACK: 0x000000,
  WHITE: 0xffffff,
  LIGHT_GREY: 0xcccccc,
  LIGHT_BLUE: 0x1099bb,
  PURPLE: 0xae5d8b,
  RED: 0xff0000,
  GREEN: 0x00ff00,
  BLUE: 0x0000ff,
  JAGGER: 0x3f3643,
};
