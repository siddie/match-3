class AppState {
  constructor() {
    console.log('Initialize global app store');
    this.data = {};
  }

  setValue(key, value) {
    this.data[key] = value;
  }

  getValue(key) {
    return this.data[key];
  }
}

return new AppState();
