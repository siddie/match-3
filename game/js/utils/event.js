// Collection of all listeners
let CollectionOfListeners = { /* Collection of EventName: [ Listeners ] */ };

CollectionOfListeners.get = function ( name ) {
  // If collection not existed - create, than return collection
  return ( ( !this[ name ] && ( this[ name ] = [] ) ) || this[ name ] );
};

CollectionOfListeners.remove = function ( name ) {
  ( this[ name ] ) && ( this[ name ] = [] );
};

const removeAll = function () {
  let events = Object.keys( CollectionOfListeners );
  for ( let i = events.length; i--; ) {
    CollectionOfListeners[ events[ i ] ].length = 0;
  }
};

const removeCustomEventListener = function ( event, func ) {
  if ( func ) {
    let coll = CollectionOfListeners.get( event );
    coll.splice( coll.indexOf( func ), 1 );
  } else {
    CollectionOfListeners.remove( event );
  }
};

const addCustomEventListener = function ( event, func ) {
  let coll = CollectionOfListeners.get( event );
  func && func.call && coll.push( func );
};

const dispatchCustomEvent = function ( event, options ) {
  let coll = CollectionOfListeners.get( event );
  for ( let i = coll.length; i--; ) {
    coll[ i ]( options );
  }
};

export const EVENTS = {
  LOADING_COMPLETE: 'LOADING_COMPLETE',
  START_NEW_GAME: 'START_NEW_GAME',
  CONTINUE: 'CONTINUE',
  BACK_TO_MENU: 'BACK_TO_MENU',
  ADD_SCORE: 'ADD_SCORE',
};

export default {
  on: addCustomEventListener,
  off: removeCustomEventListener,
  trigger: dispatchCustomEvent,
  removeAll: removeAll
};
