/**
 * Helper function to get random number from 1 to max
 * @param max
 * @returns {number}
 */
export function rand(max) {
  return ~~(Math.random() * max + 1);
}

export function between(min, max) {
  return Math.random() * (max - min) + min;
}
