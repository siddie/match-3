const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './game/launcher.js',
  output: {
   path: path.resolve(__dirname, 'public'),
   filename: 'bundle.js'
 },
 module: {
  rules: [
     {
       test: /\.js$/,
       exclude: /(node_modules)/,
       use: {
         loader: 'babel-loader',
         options: {
           presets: ['@babel/preset-env']
         }
       }
     }
  ]
 },
  stats: {
    colors: true
  },
  resolve : {
    alias: {
      'app': path.resolve(__dirname, 'game/js/app/'),
      'dictionary': path.resolve(__dirname, 'game/js/app/dictionary'),
      'main': path.resolve(__dirname, 'game/js/app/main'),
      'utils': path.resolve(__dirname, 'game/js/utils/')
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './launcher.html',
    }),
  ]
};
